# How to contribute to my projects, this included

I would be honour for you to contribute with ideas, improvements, fixes, etc... I welcome all the help I get.

Here the way to engage with me on this project:

- [Issues and Bugs](#issue)
- [Submitting a fix](#submitting)
- [Feature Requests](#features)
- [Questions](#questions)

## <a name="issue"></a> Did you find an issue?

- **Ensure the bug was not already reported** by searching on GitLab under [Issues](https://gitlab.com/CloudGod/pubsub-oci-quickstart/-/issues).

- If you're unable to find an open issue addressing the problem, [open a new one](https://gitlab.com/CloudGod/pubsub-oci-quickstart/-/issues). Be sure to include a **title and clear description**, as much relevant information as possible, and a **code sample** or an **executable test case** demonstrating the expected behavior that is not occurring. **Images are a big plus**.

## <a name="submitting"></a> Did you write a patch that fixes a bug?

Open a new GitLab merge request - MR - (roughly equivalent to a Pull Request on GitHub) with the patch following the steps outlined below. Please make sure you describe the MR as clearly as possible, covering both the problem and solution. Include the relevant issue number if applicable.

Before you submit your merge request consider the following guidelines:

- Search [GitLab](https://gitlab.com/CloudGod/pubsub-oci-quickstart/-/merge_requests) for an open or closed Pull Request
  that relates to your submission. It may be that there's one already addressing the same issue

### Submitting a Merge Request

There are many ways to create and submit a Merge Request. All of these are detailed in the [GitLab Documentation](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).

Below is one possible way to do it, derived from the information in the link above.

#### Step 1: Fork

Fork the project and clone your fork locally.

```sh
prompt:~$ git clone https://gitlab.com/CloudGod/pubsub-oci-quickstart.git
```

#### Step 2: Branch

Make your changes on a new git branch in your fork of the repository.

```sh
prompt:~$ git checkout -b my-new-branch master
```

#### Step 3: Commit

Commit your changes using a descriptive commit message.

```sh
prompt:~$ git add .
prompt:~$ git commit -m "My commit message"
```

**Note:** you can optionally commit with the `-a` command line option to automatically "add" and "rm" edited files.

#### Step 4: Push

Push your branch to Gitlab

If you have not set the upstream, do so as follows:

```sh
prompt:~$ git push origin my-new-branch
```

n the output, GitLab prompts you with a direct link for creating a merge request:

```sh
...
remote: To create a merge request for docs-new-merge-request, visit:
remote:   https://gitlab-instance.com/my-group/my-project/merge_requests/new?merge_request%5Bsource_branch%5D=my-new-branch
```

Copy that link and paste it in your browser, and the New Merge Request page is displayed. Fill in the remaining details there.

**Note:** Please notice that you can use more push options to reduce the need for manual UI entry to create a Merge Request. Please find some [hints here](https://docs.gitlab.com/ee/user/project/push_options.html).

And... That's it! Thank you for your help!

## <a name="features"></a> **Do you have an ideas for a new feature or a change to an existing one?**

- Open a GitLab [enhancement request issue](https://gitlab.com/CloudGod/pubsub-oci-quickstart/-/issues) and describe the new functionality. Don't forget to label it as an **enhancement**.

##  <a name="questions"></a> Do you have questions about the source code?

- With regards to this specific project, please visit the [Solace community](https://solace.community/) and place your question there. I lurk aroung with the @CloudGod handle and I'm always eager to help.
