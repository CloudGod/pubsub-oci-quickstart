#!/bin/bash
##################################
# Update following variables as needed:
SOLACE_DOCKER_IMAGE_REFERENCE="solace/solace-pubsub-standard:latest" # Default to pull latest PubSub+ standard from docker hub
ADMIN_PASSWORD="admin-password"                                      # Update to a real password
INSTALL_SCRIPT="https://gitlab.com/CloudGod/pubsub-oci-quickstart/raw/master/scripts/install-solace.sh"
# NOTE: tHESE IPs MUST BE THE SAME IN ALL NODES OF THE HA CLUSTER!
PRIMARY_IP=10.0.0.100   # Primary IP - Change to an IP within the CIDR block range of your subnet
BACKUP_IP=10.0.0.101    # Backup IP - Change to an IP within the CIDR block range of your subnet
MONITOR_IP=10.0.0.102   # Monitor IP - Change to an IP within the CIDR block range of your subnet
##################################
export baseroutername=ocivmr # Change the name of the Virtual Message Router (VMR) to whatever you like. Makesure it's coherent in the remaining "export" commands.
export nodetype=monitoring   # This node is for monitoring
export routername=ocivmr2    # Use the same as baseroutername followed by 2
export redundancy_enable=yes
export redundancy_group_password=groupadminpass # group password. keep the same across all HA configurations.
export redundancy_group_node_ocivmr0_connectvia=$PRIMARY_IP   # Notice the group node name has the baseroutername inside. change accordingly.
export redundancy_group_node_ocivmr0_nodetype=message_routing # Notice the group node name has the baseroutername inside. change accordingly.
export redundancy_group_node_ocivmr1_connectvia=$BACKUP_IP    # Notice the group node name has the baseroutername inside. change accordingly.
export redundancy_group_node_ocivmr1_nodetype=message_routing # Notice the group node name has the baseroutername inside. change accordingly.
export redundancy_group_node_ocivmr2_connectvia=$MONITOR_IP   # Notice the group node name has the baseroutername inside. change accordingly.
export redundancy_group_node_ocivmr2_nodetype=monitoring      # Notice the group node name has the baseroutername inside. change accordingly.
##################################
#
if [ ! -d /var/lib/solace ]; then
  mkdir /var/lib/solace
  cd /var/lib/solace
  LOOP_COUNT=0
  while [ $LOOP_COUNT -lt 30 ]; do
    yum install -y wget || echo "yum not ready, waiting"
    wget $INSTALL_SCRIPT
    if [ 0 != `echo $?` ]; then 
      ((LOOP_COUNT++))
    else
      break
    fi
  done
  if [ ${LOOP_COUNT} == 30 ]; then
    echo "`date` ERROR: Failed to download initial install script - exiting"
    exit 1
  fi
  cd /etc/yum/vars
  echo 7 > releasever
  cd /var/lib/solace
  chmod +x /var/lib/solace/install-solace.sh
  /var/lib/solace/install-solace.sh -p $ADMIN_PASSWORD -i $SOLACE_DOCKER_IMAGE_REFERENCE
fi
