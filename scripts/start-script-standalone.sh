#!/bin/bash
##################################
# Update following variables as needed:
SOLACE_DOCKER_IMAGE_REFERENCE="solace/solace-pubsub-standard:latest" # Default to pull latest PubSub+ standard from docker hub
ADMIN_PASSWORD="admin-password"                                      # Update to a real password
INSTALL_SCRIPT="https://gitlab.com/CloudGod/pubsub-oci-quickstart/raw/master/scripts/install-solace.sh"
##################################
# Add here environment variables for HA deployment, not required for single-node deployment.
# export ... see next section HA deployment environment variables
##################################
#
if [ ! -d /var/lib/solace ]; then
  mkdir /var/lib/solace
  cd /var/lib/solace
  LOOP_COUNT=0
  while [ $LOOP_COUNT -lt 30 ]; do
    yum install -y wget || echo "yum not ready, waiting"
    wget $INSTALL_SCRIPT
    if [ 0 != `echo $?` ]; then 
      ((LOOP_COUNT++))
    else
      break
    fi
  done
  if [ ${LOOP_COUNT} == 30 ]; then
    echo "`date` ERROR: Failed to download initial install script - exiting"
    exit 1
  fi
  cd /etc/yum/vars
  echo 7 > releasever
  cd /var/lib/solace
  chmod +x /var/lib/solace/install-solace.sh
  /var/lib/solace/install-solace.sh -p $ADMIN_PASSWORD -i $SOLACE_DOCKER_IMAGE_REFERENCE
fi
