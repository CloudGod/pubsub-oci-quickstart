# Install a Solace PubSub+ Software Event Broker onto Oracle Cloud Infrastructure (OCI) 2nd. Gen Linux Virtual Machines

## Purpose of this repository

This repository explains how to install a Solace PubSub+ Software Event Broker in various configurations onto Oracle's OCI Compute Linux Virtual Machines. This guide is intended for development and demo purposes.

This guide was built and tested using Solace PubSub+ Message Broker version 9.7.0.29.

**Important Note:** This guide was made using [Solace's guide to install the Solace Event Broker on Google's Compute Engine (GCE)](https://github.com/SolaceProducts/pubsubplus-gcp-quickstart) as a basis. A lot of the text is copied directly (no need to reinvent the wheel) with only the parts that are specific to Oracle's OCI changed. Most of the credit is due to the authors of that guide. I merely followed the instructions and adapted them to OCI.

## Description of the Solace PubSub+ Software Event Broker

The [Solace PubSub+ Platform](https://solace.com/products/platform/)'s [PubSub+ Software Event Broker](https://solace.com/products/event-broker/software/) efficiently streams event-driven information between applications, IoT devices, and user interfaces running in the cloud, on-premises, and hybrid environments using open APIs and protocols like AMQP, JMS, MQTT, REST, and WebSocket. It can be installed into a variety of public and private clouds, PaaS, and on-premises environments, and brokers in multiple locations can be linked together in an [event mesh](https://solace.com/what-is-an-event-mesh/) to dynamically share events across the distributed enterprise.

## How to deploy Solace PubSub+ Software Event Broker onto Oracle OCI

Solace PubSub+ can be deployed in either a three-node High-Availability (HA) group or as a single-node Standalone deployment. For simple test environments that need only to validate application functionality, a single instance will suffice. Note that in production, or any environment where message loss cannot be tolerated, a HA deployment is required.

## Step 1 (Optional): Obtain a reference to the Docker image of the Solace PubSub+ Software Event Broker to be deployed

First, decide which [Solace PubSub+ Software Event Broker type](https://docs.solace.com/Solace-SW-Broker-Set-Up/Setting-Up-SW-Brokers.htm ) and version is suitable for your use case.

> **Note:** You can skip the rest of this step if you're using the default settings. By default, this project installs the Standard edition of the Solace PubSub+ Software Event Broker from the latest Docker image available from Docker Hub.

The Docker image reference can be:

*	A public or accessible private Docker registry repository name with an optional tag. This is the recommended option if using PubSub+ Software Event Broker Standard. The default is to use the latest PubSub+ image [available from Docker Hub](https://hub.docker.com/r/solace/solace-pubsub-standard/ ) as `solace/solace-pubsub-standard:latest`, or use a specific version [tag](https://hub.docker.com/r/solace/solace-pubsub-standard/tags/ ).

*	A Docker image download URL
     * If using Solace PubSub+ Software Event Broker Enterprise Evaluation Edition, go to the Solace Downloads page. For the image reference, copy and use the download URL in the Solace PubSub+ Software Event Broker Enterprise Evaluation Edition Docker Images section.

         | PubSub+ Software Event Broker Enterprise Evaluation Edition<br/>Docker Image
         | :---: |
         | 90-day trial version of PubSub+ Enterprise |
         | [Get URL of Evaluation Docker Image](https://products.solace.com/download/PUBSUB_DOCKER_EVAL) |

     * If you have purchased a Docker image of Solace PubSub+ Software Event Broker Enterprise, Solace will give you information on how to download the compressed tar archive package from a secure Solace server. Contact Solace Support at support@solace.com if you require assistance. You can then host this tar archive together with its MD5 on a file server and use the download URL as the image reference.

## Step 2: Create the required OCI Compute instances

The single stand-alone instance requires 1 Compute instance, and the HA deployment requires 3 instances for the Primary, Backup, and Monitor nodes.

Repeat these instructions for all instances required, and follow the specific requirements for HA setup as applicable.

### Step 2-1: Setup the Compartment and Virtual Cloud Network

* Go to your Oracle OCI Console and create a new Compartment, where you'll place all your services so that they're organized. You do it by going into the "hamburger" Menu > Identity > Compartments and then click on "Create Compartment".

![alt text](images/oci_createcompartment_0.png "OCI Compartments Area")

![alt text](images/oci_createcompartment_1.jpg "OCI Create Compartment Option")

* Fill in the compartment name, description, and its parent compartment, and click on "Create Compartment"

![alt text](images/oci_createcompartment_3.jpg "OCI Create Compartment Form")

Your compartment should now be created.

![alt text](images/oci_createcompartment_4.png "OCI Create Compartment Form")

Onwards with the infrastructure setup.

* Start by creating a virtual cloud network. This guide considers the need to have internet bi-directional communication, so we will have both public and private virtual networks. You can do this easily through the OCI Console home, using the VCN wizard.
  * If you have other security requirements, adjust accordingly.

![alt text](images/oci_setupavirtualnetwork_1.png "OCI Set Up a Virtual Network")

![alt text](images/oci_setupavirtualnetwork_2.png "OCI Set Up a Virtual Network")

* The wizard is pretty straightforward. Just give the VCN a name and place it under the compartment you created before. You can adjust the networking CIDR blocks as you wish, to fit your networking needs, or you can leave it with the default values. Fill in the necessary information, click "Next" to review it, and click "Create" to create your VCN.
![alt text](images/oci_setupavirtualnetwork_3.png "OCI Create your VNC")

![alt text](images/oci_setupavirtualnetwork_4.png "OCI Review the creation form and create the VCN")

![alt text](images/oci_setupavirtualnetwork_5.png "OCI Created")

* Your dedicated VCN is now ready to be used and configured.

### Step 2-1a: HA Deployment only

The next step is to check the **CIDR block** of your private IPs of your "Public Subnet", the one we will be using in this guide. This tells you what range of private IPs you can allocate to your machines. This is important when defining your HA deployment.
> Note: Your **Public** subnet will have its range of **Private** IPs as well. The machines that use this subnet can also have a **Public** IP allocated. Don't confuse the terms.

Analyze the CIDR block and choose 3 IPs within that block. In the example below, the CIDR block is 10.0.0.0/24, which roughly equates to an IP range between 10.0.0.0-10.0.0.255 (**in practice, between 10.0.0.1-10.0.0.254**, as the '0' and the '255' are reserved in OCI), so these are the addresses your machines can take.
> Tip: If you have problems checking the IP Range in a CIDR block, you can use a [CIDR block calculator](https://mxtoolbox.com/subnetcalculator.aspx).

![alt text](images/oci_cidrblock_1.png "CIDR block")

You can choose any 3 IPs that aren't already taken inside that subnet. Write them down somewhere as `<Primary IP>`, `<Backup IP>`, and `<Monitor IP>`. You'll need them later when creating your compute instances for the HA deployment.
> Tip: **You won't need them if want a standalone deployment. Just let OCI set the Private IP for you**

### Step 2-2: Determine the resource requirements of the compute instances

Determine the PubSub+ resource requirements based on the targeted [connection scaling](https://docs.solace.com/Configuring-and-Managing/SW-Broker-Specific-Config/System-Resource-Requirements.htm)

At a minimum, select standard 1 OCPU machine type (equates to 2 threads/vCPUs), and at least 2 GB of memory, an Oracle Linux 7 OS, and a persistent disk with a size of at least 30 GB. If you can, go for the default shape (VM.Standard.E3.Flex, with 1 OCPU and 16 GB of RAM).

**Note:** In an HA deployment it's recommended to choose a different availability zone for each node. Also, the Monitor node requires only 10 GB of disk space.

### Step 2-3: Create your Compute Instances
* Use either the direct option in the OCI console homepage or go to the menu and choose Compute > Instances > Create Instance, to create your compute instances. The console will present the compute instance creation form.
  * If you want to have a HA deployment, you'll need to repeat these steps for all three nodes, together with the additional instructions from Step 2-3a.

![alt text](images/oci_createinstance_1.png "OCI Compute instance creation")

* Choose the instance name, the compartment in which it will be created, the availability domain, the image to use, and the shape of the compute instance.

![alt text](images/oci_createinstance_2.png "OCI Create Instance Form")

* In the "Configure networking" part, select the VCN you created before and choose the public subnet within. Leave the "Assign a Public IP Address" option on, so that you can reach your solace broker from the internet.

![alt text](images/oci_createinstance_3.png "OCI Configure Instance Networking")

* Generate your SSH keys or just download the ones supplied by OCI. Leave the boot volume options as default, or adjust accordingly to the requirements specified in Step 2-2. Then click on "Show Advanced Options"

![alt text](images/oci_createinstance_4.png "OCI Configure SSH Access and volume size")

* In the "Management" tab of the Advanced Options, add an automated start script. Choose the "Paste Cloud-Init Script" option and paste the script inside the field. Leave all other options as default.

![alt text](images/oci_createinstance_5.png "OCI Set up the Start Script")
>Tip: You can find example start scripts according to the deployment type you want ([Standalone](/scripts/start-script-standalone.sh) or HA [HA-Primary](scripts/start-script-ha-primary.sh) - [HA-Backup](scripts/start-script-ha-backup.sh) - [HA-Monitor](scripts/start-script-ha-monitor.sh)) on this repository.

* Go to the Networking tab and choose the "Hardware-Assisted (SR-IOV) Networking", which is optimized for real-time event traffic.

* Click on "Create" to create the instance. Your compute instance is now being created.
> Tip: It will take some time to install everything. You can check the progress by SSH'ing into the machine and checking the /var/lib/solace/install.log file. Example:
```shell
Mon Oct 19 18:34:21 GMT 2020 INFO: Validate we have been passed a Solace Docker Image reference
Mon Oct 19 18:34:21 GMT 2020 INFO: Solace Docker image reference is solace/solace-pubsub-standard:latest
Mon Oct 19 18:34:21 GMT 2020 INFO: Get repositories up to date
Mon Oct 19 18:45:37 GMT 2020 INFO:Set up Docker Repository
Mon Oct 19 18:45:50 GMT 2020 INFO:Intall Docker
Mon Oct 19 18:46:26 GMT 2020 INFO:Configure Docker as a service
Mon Oct 19 18:46:34 GMT 2020 INFO: Docker in state running
Mon Oct 19 18:46:34 GMT 2020 INFO: Get the solace image
Mon Oct 19 18:46:34 GMT 2020 Testing solace/solace-pubsub-standard:latest for docker registry uri:
Mon Oct 19 18:46:56 GMT 2020 INFO: Successfully loaded solace/solace-pubsub-standard:latest to local docker repo
Mon Oct 19 18:46:56 GMT 2020 INFO: Solace message broker image and tag: solace/solace-pubsub-standard : latest
Mon Oct 19 18:46:56 GMT 2020 INFO:Set up swap
Mon Oct 19 18:46:56 GMT 2020 INFO: Based on memory size of 16148244KiB, determined maxconnectioncount: 1000, shmsize: 2g, ulimit_nofile: 2448:42192, SWAP_SIZE: 2048
Mon Oct 19 18:46:56 GMT 2020 INFO: Creating Swap space
Mon Oct 19 18:47:47 GMT 2020 INFO: Applying TCP for WAN optimizations
Mon Oct 19 18:47:47 GMT 2020 INFO:Create a Docker instance from Solace Docker image
SOLACE_CLOUD_INIT set to:
--env service_ssh_port=2222 --env service_webtransport_port=8008 --env service_webtransport_tlsport=1443 --env service_semp_tlsport=1943 --env username_admin_globalaccesslevel=admin --env username_admin_password=&%5gZN$%
Mon Oct 19 18:47:58 GMT 2020 INFO:Construct systemd for Solace PubSub+
Mon Oct 19 18:47:58 GMT 2020 INFO:/etc/systemd/system/solace-docker.service =/n [Unit]
  Description=solace-docker
  Requires=docker.service
  After=docker.service
[Service]
  Restart=always
  ExecStart=/usr/bin/docker start -a solace
  ExecStop=/usr/bin/docker stop solace
[Install]
  WantedBy=default.target
Mon Oct 19 18:47:58 GMT 2020 INFO: Start the Solace Message Router
Mon Oct 19 18:47:58 GMT 2020 INFO: Install is complete
```

* You'll need to open up ports in your VCN to allow traffic coming from the internet to reach your instance. This is done in two stages: 
  * The OCI security list for your public subnet
  * The compute instance firewall

> Important note: This guide is not intended to set up security according to the best practices. You should do that for yourself. To ensure that Solace was properly installed, you can loose restrictions on both stages.
**SET UP PROPER SECURITY RULES FOR ALL ENVIRONMENTS, ONCE YOU ESTABLISH EVERYTHING IS OK**

* Go to the OCI Console menu, Networking > Virtual Cloud Networks, and click on the VCN you created before

![alt text](images/oci_security_1.png "OCI List of VCNs")

![alt text](images/oci_security_2.png "OCI Choose your VCN")

* Choose the subnet that your solace instances use.

![alt text](images/oci_security_3.png "OCI Select the Subnet")

* Access the Security List attached to it.

![alt text](images/oci_security_4.png "OCI Select the Security List")

* The default security list is pretty much closed to the outside. Add a new Ingress Rule which will allow traffic to come from the Internet to your solace instances.

![alt text](images/oci_security_5.png "OCI Add Ingress Rule")

* For the sake of simplicity, you can set the machine to allow all traffic from the outside. Use this config only while setting things up to make sure everything works. 

![alt text](images/oci_security_6.png "OCI Open all Ports")

* Now you have OCI accepting incoming requests from the Internet.

![alt text](images/oci_security_7.png "OCI Security List Ingress Rules set")

> Important Note: After making sure everything is running fine, close down the Security List to only allow traffic to the ports used by solace. Typically, [these are](https://docs.solace.com/Configuring-and-Managing/Default-Port-Numbers.htm#Software):

``` shell
22, 2222, 8080, 1943, 5550, 55555, 55003, 55556, 55443, 8008, 1443, 5671, 5672, 1883, 8883, 8000, 8443, 9000, 9443, 8741, 8300, 8301, 8302
```

* Finally, you need to set up your instance firewall, which comes active by default on Oracle Linux, so that it accepts the necessary traffic. Again, for this guide purpose, which is to set everything up and running, just proceed with the firewall deactivation. You should set the appropriate firewall rules after you made sure everything is in running order.
  * Access your instance(s) through ssh and just shut down the firewall service.

``` shell
systemctl stop firewalld    # Stops the service
systemctl disable firewalld # Disables the service so that it doesn't restart when the machine reboots.
```

By this time, you should be able to access Solace's login screen of the management console, through your browser, using the public IP and port 8080.

### Step 2-3a: (HA cluster deployment only) Customize your Private IP addresses

If you are configuring three HA nodes, you need to take some extra steps, so that all the nodes are correctly configured and 'talking' with each other.

For each of the nodes:

* Choose a different Availability Domain for each node. For instance, place the primary no on AD1, the Backup on AD2, and the monitor on AD3.

![alt text](images/oci_createinstance_ha_1.png "OCI set different Availability Domains for each node")

* Go to the Networking Tab and fill in the "Private IP" field with the corresponding IP, depending on the node role, from the 3 IPs written down previously:`<PrimaryIP>`, `<BackupIP>`, and `<MonitorIP>`.

![alt text](images/oci_createinstance_ha_2.png "OCI set the corresponding private IP")

* Use one of the supplied start scripts adjusted for each node type, adjusting the IPs variable definitions:
  * [Primary node start script](https://gitlab.com/CloudGod/pubsub-oci-quickstart/raw/master/scripts/start-script-ha-primary.sh)
  * [Backup node start script](https://gitlab.com/CloudGod/pubsub-oci-quickstart/raw/master/scripts/start-script-ha-backup.sh)
  * [Monitor node start script](https://gitlab.com/CloudGod/pubsub-oci-quickstart/raw/master/scripts/start-script-ha-monitor.sh)

![alt text](images/oci_createinstance_ha_3.png "OCI Set the appropriate start script for each node")

![alt text](images/oci_createinstance_ha_4.png "OCI Set the IPs in the startup script")

* Copy and paste the appropriate startup script for each node, into the "Management" tab
![alt text](images/oci_createinstance_5.png "OCI Set up the Start Script")

Create the respective nodes. After a few minutes (10-15) you should be up and running. Keep checking the ` /var/lib/solace/install.log`
## Step 3: (HA cluster deployment only) Assert the primary event broker’s configuration

As described in the [Solace documentation for configuring HA Group](https://docs.solace.com/Configuring-and-Managing/Configuring-HA-Groups.htm ) it's required to assert the primary event broker’s configuration after a Solace PubSub+ HA redundancy group is configured to support Guaranteed messaging. This can be done through Solace CLI commands as in the [documentation](https://docs.solace.com/Configuring-and-Managing/Configuring-HA-Groups.htm#Config-Config-Sync ) or running the following commands at the Primary node (replace `<ADMIN_PASSWORD>` according to your settings):

```shell
# query redundancy status
curl -sS -u admin:<ADMIN_PASSWORD> http://localhost:8080/SEMP -d "<rpc><show><redundancy></redundancy></show></rpc>"

# wait until redundancy is up then execute the assert command:

curl -sS -u admin:<ADMIN_PASSWORD> http://localhost:8080/SEMP -d "<rpc><admin><config-sync><assert-master><router/></assert-master></config-sync></admin></rpc>"
```

After this, your HA cluster is set up and running.

> Tip: If the `curl` command returns nothing, you're probably supplying the wrong password. Double-check the startup scripts to make sure it's correct.

# Gaining admin access to the event broker

Refer to the [Management Tools section](https://docs.solace.com/Management-Tools.htm ) of the online documentation to learn more about the available tools. The Solace PubSub+ Manager is the recommended way to administer the event broker for common tasks.

## PubSub+ Manager, SolAdmin, and SEMP access

The Management IP will be the Public IP associated with your OCI instance, and the port will be 8080 by default.

**Note:** If using the HA deployment, unless specifically required otherwise, use the OCI instance that is in the Active role (this is the Primary node at the initial setup, but can be the Backup node after a failover).

## Solace CLI access

Connect to your instance through SSH, then launch a Solace CLI session using the command:

```shell
$sudo docker exec -it solace /usr/sw/loads/currentload/bin/cli -A
```
You'll enter the Solace CLI to query and configure the event broker, including setting up the certificates for secure connections (more on this later)

``` shell

Solace PubSub+ Standard Version 9.7.0.29

The Solace PubSub+ Standard is proprietary software of
Solace Corporation. By accessing the Solace PubSub+ Standard
you are agreeing to the license terms and conditions located at
http://www.solace.com/license-software

Copyright 2004-2020 Solace Corporation. All rights reserved.

To purchase product support, please contact Solace at:
https://solace.com/contact-us/

Operating Mode: Message Routing Node

solace-ha-primary>
```

# Testing data access to the event broker

The easiest way to test if things are OK is to use the "Try Me!" functionality in the Management console. To make it more comprehensive, get a [Solace Cloud PubSub+ account](https://console.solace.cloud/login/new-account) and create a basic VPN.

All the connection details are available in a "Connect" tab, in the service details.

![alt text](images/oci_testing_0.png "Get the Solace Cloud Connection details to a VPN")

To test things, use the "Try Me!" functionality on the OCI Management console, to send a message from your recently set up instance to the one in the Solace cloud. The other way around (from the cloud to your OCI Instance) will require you to set up a certificate in your OCI broker. Again, this guide will address this later.

* Access the Management Console on your OCI instance with the URL `<PublicIP>:8080`, using the credentials set before, and choose your VPN (probably called "Default"). Then go to the "Try Me!" functionality.

* Open up the connection details area on the **Publisher** side, if it's not opened yet, and fill in the details of the connection to your Solace Cloud event broker. This is where you want to publish. Click on Connect. you should see a "Connected" indication. If not, you probably have not set up your OCI networking to access the internet.

* Choose the topic and write a message, but don't send it yet.

![alt text](images/oci_testing_1.png "Connect to the Solace Cloud and prepare the message to send. Don't send it yet!")

* Go to another browser window and open up the "Try Me!" functionality in the Solace Cloud event Broker. Login into the Solace Cloud, Cluster Manager > `<Select your service>` > Manage > Message VPN. Then go to the "Try Me!" functionality.

![alt text](images/oci_testing_3.png "Solace Cloud Message VPN")

* Open up the connection details area on the **Subscriber** side, if it's not opened yet, and fill in the details of the connection to your Solace Cloud event broker. This is where messages will be picked up from. Click on Connect. you should see a "Connected" indication.

![alt text](images/oci_testing_4.png "Solace Cloud Subscribe")

* Finally, make sure the topic that you're publishing the message to is a topic that is subscribed by the Subscriber. You need to click on the subscribe button, in the subscriber area, to make the subscription effective. Of course, it's possible to subscribe to more than one topic.

* Once the subscription is in place, you can send the message. If everything is correct, the Solace Cloud event broker will get the message.

![alt text](images/oci_testing_2.png "Message sent. Test Ok")

---
You can also test data traffic through the newly created event broker instance, visit the Solace Developer Portal and select your preferred programming language, to [send and receive messages](http://dev.solace.com/get-started/send-receive-messages/). Under each language there is a Publish/Subscribe tutorial that will help you get started and provide the specific default port to use.

For single-node configuration, the IP to use will be the External IP associated with your GCE instance. For HA deployment the use of [Client Host List](https://docs.solace.com/Features/SW-Broker-Redundancy-and-Fault-Tolerance.htm#Failover ) is required for seamless failover - this will consist of the External IP addresses associated with your Primary and Backup node OCI instances.

![alt text](images/solace_tutorial.png "getting started publish/subscribe")

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests.

## Authors

See the list of [contributors](https://gitlab.com/CloudGod/pubsub-oci-quickstart/-/graphs/master) who participated in this project.
Also, the [contributors](https://github.com/SolaceProducts/solace-gcp-quickstart/graphs/contributors) who participated in the [Solace GCP Quickstart](https://github.com/SolaceProducts/pubsubplus-gcp-quickstart)

## License

This project is licensed under the Apache License, Version 2.0. - See the [LICENSE](LICENSE) file for details.

## Resources

For more information about Solace technology in general please visit these resources:

* The Solace Developer Portal website at [solace.dev](https://solace.dev/)
* Understanding [Solace technology](https://solace.com/products/platform/)
* Ask the [Solace community](https://solace.community/).
